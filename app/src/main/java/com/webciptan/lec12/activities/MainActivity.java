package com.webciptan.lec12.activities;

import android.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

import com.webciptan.lec12.R;
import com.webciptan.lec12.fragments.FirstFragment;
import com.webciptan.lec12.fragments.SecondFragment;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    FirstFragment firstFragment;
    SecondFragment secondFragment;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        firstFragment = new FirstFragment();
        secondFragment = new SecondFragment();
        findViewById(R.id.btnAddFirstActivity).setOnClickListener(this);
        findViewById(R.id.btnRemoveFirstActivity).setOnClickListener(this);
        findViewById(R.id.btnAddSecondActivity).setOnClickListener(this);
        findViewById(R.id.btnRemoveSecondActivity).setOnClickListener(this);


    }
    public void onClick(View view)
    {
        FragmentTransaction transaction =  getFragmentManager().beginTransaction();
    switch (view.getId())
      {
          case R.id.btnAddFirstActivity:
              Toast toast = Toast.makeText(getApplicationContext(),
                      "Пора покормить кота!", Toast.LENGTH_SHORT);
              toast.show();
              if(firstFragment.isAdded())
              {
                  transaction.replace(R.id.frameLayout1, firstFragment);
              }
              else
              {
                  transaction.add(R.id.frameLayout1, firstFragment);
              }
              break;
          case R.id.btnRemoveFirstActivity:
              toast = Toast.makeText(getApplicationContext(),
                      "Пора убить кота!", Toast.LENGTH_SHORT);
              toast.show();
                transaction.remove(firstFragment);
              break;
          case R.id.btnAddSecondActivity:
              toast = Toast.makeText(getApplicationContext(),
                      "Пора покормить кота 2", Toast.LENGTH_SHORT);
          toast.show();
          if(secondFragment.isAdded())
          {
              transaction.replace(R.id.frameLayout1, secondFragment);
          }
          else
          {
              transaction.add(R.id.frameLayout1, secondFragment);
          }
          break;
          case R.id.btnRemoveSecondActivity:
              toast = Toast.makeText(getApplicationContext(),
                      "Пора убить кота 2!", Toast.LENGTH_SHORT);
              toast.show();
              transaction.remove(secondFragment);
              break;
      }
        transaction.commit();
    }

    public void sendData(String string)
    {
          secondFragment.setData(string);
    }
}
