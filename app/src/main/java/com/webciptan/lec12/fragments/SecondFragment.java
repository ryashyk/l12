package com.webciptan.lec12.fragments;

import android.app.Fragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.webciptan.lec12.R;

/**
 * Created by Роман on 19.04.2017.
 */


public class SecondFragment extends Fragment {
    private TextView textView;
    private String strData;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_second,container,false);
        textView = (TextView) view.findViewById(R.id.textViewDataSecondFragment);
        if(strData !=null)
        {
            textView.setText(strData);
        }


        return view;
    }
    public void setData(String strData)
    {
        this.strData = strData;
        if(isAdded() && textView!=null)
        {
            textView.setText(strData);
        }
    }
}
